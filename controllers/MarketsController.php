<?php

namespace app\controllers;

use Yii;
use app\models\Markets;
use app\models\MarketsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use arturoliveira\ExcelView;
/**
 * MarketsController implements the CRUD actions for Markets model.
 */
class MarketsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Markets models.
     * @return mixed
     */
      public function actionExport() {
        $searchModel =   new MarketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        ExcelView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'fullExportType'=> 'xlsx', //can change to html,xls,csv and so on
            'grid_mode' => 'export',
            'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],

            //'market_id',
            'market_code',
            'market_name',
            'market_location',
            'market_created_date',

              ],
        ]);
    }
    public function actionIndex()
    {
         $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $searchModel = new MarketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
        'pageSize' => 8,
    ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Markets model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         $this->layout = "@yiister/gentelella/views/layouts/main.php";
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Markets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $model = new Markets();

        if ($model->load(Yii::$app->request->post())) {
            $model->market_created_date = new Expression('NOW()');
            $model->save();
            return $this->redirect(['view', 'id' => $model->market_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Markets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->market_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Markets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Markets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Markets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Markets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
