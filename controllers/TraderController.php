<?php

namespace app\controllers;

use Yii;
use app\models\Trader;
use app\models\Tradercommodities;
use app\models\TraderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use linslin\yii2\curl;
use yii\web\Session;
use yii\data\Pagination;
use arturoliveira\ExcelView;


/**
 * TraderController implements the CRUD actions for Trader model.
 */
class TraderController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trader models.
     * @return mixed
     */
      public function actionExport() {
        $searchModel =  new TraderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        ExcelView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'fullExportType'=> 'xlsx', //can change to html,xls,csv and so on
            'grid_mode' => 'export',
            'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],
            [ 'attribute'=>'market_id',
             'value'=>'market.market_name',
                
            ],
            
            //'trader_id',
            'fName',
            'sName',
            'email',
            'id_No',
            'date',
            'phone',
            'stall_No',
            'passport_photo_no',
            'trader_commodity_id',
              ],
        ]);
    }

    public function actionIndex() {


        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $searchModel = new TraderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
        'pageSize' => 8,
    ];

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trader model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Trader model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $session = Yii::$app->session;
        $model = new Trader();
        $trader_commodity=array();
       
        
        if ($model->load(Yii::$app->request->post())) {
            $trader_commodity = $_POST['Trader']['trader_commodity_id'];
            
           // var_dump($trader_commodity); exit;
            //foreach($trader_commodity as $key => $value ):
        
            $model->date = new Expression('NOW()');
            //$trader_commodity = json_encode($_POST['Trader']['trader_commodity_id']);
            
           
           //echo $trader_commodity; exit;
           //$trader_commodity = preg_replace('/\\\"/',"\"",$trader_commodity);
            $model->trader_commodity_id = implode(',',$trader_commodity);
            
           
//            $curl = new curl\Curl();
//            $response = $curl->setOption(
//                            CURLOPT_POSTFIELDS, http_build_query(array(
//                        'grant_type' => "agency",
//                        'username' => "teller1.branch1@agency.com",
//                        'password' => 'p@ssw0rd'
//                                    )
//                    ))
//                    ->post('http://192.168.11.22/jambopayservices/token');
//            $res = json_decode($response);
//            if ($res !== NULL) {
//                $token = $res->access_token;
//
//
//                $request = Yii::$app->request->post();
//                $username = $request['Trader']['phone'];
//                $pino = '0000';
//                $email = $request['Trader']['email'];
//                $firstname = $request['Trader']['fName'];
//                //var_dump($request['Trader']['fName']); die();
//                $secondname = $request['Trader']['sName'];
//                $idno = $request['Trader']['id_No'];
//                $params = [
//                    'Stream' => "wallet",
//                    'PhoneNumber' => $username,
//                    'Pin' => $pino,
//                    'Email' => $email,
//                    'FirstName' => $firstname,
//                    'LastName' => $secondname,
//                    'IDNumber' => $idno
//                ];
//                $headers = [
//                    'Authorization: bearer ' . $token,
//                    'app_key: 7E9D236F-67E2-E411-8285-B8EE657EAEBC'
//                ];
//
//
//                $resWallet = $curl->setOption(
//                                CURLOPT_POSTFIELDS, http_build_query($params
//                        ))
//                        ->setOption(CURLOPT_HTTPHEADER, $headers)
//                        ->post('http://192.168.11.22/jambopayservices/api/Wallet/PostWalletRegister');
//                $session = Yii::$app->session;
//
//
//                $session['error_object'] = json_decode($resWallet);
//                //var_dump($session['error_object']); die();
//                $error = isset($session['error_object']) ? $session['error_object'] : null;
//                //var_dump($error->ErrorType); die();
//                if (isset($error->ErrorType)) {
//                    \Yii::$app->getSession()->setFlash('error-message', $error->Message);
//                    $this->redirect(['create']);
//                } else {
//                    \Yii::$app->getSession()->setFlash('success-message', "You have successfully created a Jambopay wallet");
//                    
//                }
             
         
                 if($model->save()){
                     $trader_id=$model->getPrimaryKey();
                     
                     foreach($trader_commodity as $key => $val)
                         {
                         $commoditiesModel=new Tradercommodities();
                         $commoditiesModel->trader_id=$trader_id;
                         $commoditiesModel->trader_id_no=$model->id_No;
                         $commoditiesModel->traderId=  rand(10000, 100000);
                         $commoditiesModel->commodity_id=7;
                         $commoditiesModel->save();                        
                     }
                      return $this->redirect(['view', 'id' => $model->trader_id]);
                     
                 }
                
                    
         
      
        
        //endforeach;
        
         
        }
         else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        
        }
        
    }

    /**
     * Updates an existing Trader model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trader_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Trader model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
       $this->layout = "@yiister/gentelella/views/layouts/main.php";
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trader model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trader the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Trader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
