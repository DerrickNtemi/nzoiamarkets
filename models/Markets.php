<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "markets".
 *
 * @property integer $market_id
 * @property integer $market_code
 * @property string $market_name
 * @property string $market_location
 * @property string $market_created_date
 *
 * @property Trader[] $traders
 */
class Markets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'markets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['market_code', 'market_name', 'market_location', 'market_created_date'], 'required'],
            [['market_code'], 'integer'],
            [['market_created_date'], 'safe'],
            [['market_name', 'market_location'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'market_id' => 'Market ID',
            'market_code' => 'Market Code',
            'market_name' => 'Market Name',
            'market_location' => 'Market Location',
            'market_created_date' => 'Market Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraders()
    {
        return $this->hasMany(Trader::className(), ['market_id' => 'market_id']);
    }
}
