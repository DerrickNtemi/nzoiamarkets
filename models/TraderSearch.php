<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trader;

/**
 * TraderSearch represents the model behind the search form about `app\models\Trader`.
 */
class TraderSearch extends Trader
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trader_id', 'id_No'], 'integer'],
            [['fName', 'sName', 'date', 'phone', 'stall_No','email', 'passport_photo_no', 'market_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trader::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('market');

        // grid filtering conditions
        $query->andFilterWhere([
            'trader_id' => $this->trader_id,
            'id_No' => $this->id_No,
            'date' => $this->date,
            'email'=> $this->email,
           
        ]);

        $query->andFilterWhere(['like', 'fName', $this->fName])
            ->andFilterWhere(['like', 'sName', $this->sName])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'stall_No', $this->stall_No])
            ->andFilterWhere(['like', 'passport_photo_no', $this->passport_photo_no])
         ->andFilterWhere(['like', 'markets.market_name', $this->market_id]);

        return $dataProvider;
    }
}
