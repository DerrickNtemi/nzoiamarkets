<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commodities".
 *
 * @property integer $commodity_id
 * @property integer $commodity_code
 * @property string $commodity_name
 * @property string $commodity_created_date
 */
class Commodities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commodities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commodity_code', 'commodity_name', 'commodity_created_date'], 'required'],
            [['commodity_code'], 'integer'],
            [['commodity_created_date'], 'safe'],
            [['commodity_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'commodity_id' => 'Commodity ID',
            'commodity_code' => 'Commodity Code',
            'commodity_name' => 'Commodity Name',
            'commodity_created_date' => 'Commodity Created Date',
        ];
    }
}
