<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Commodities;

/**
 * CommoditiesSearch represents the model behind the search form about `app\models\Commodities`.
 */
class CommoditiesSearch extends Commodities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commodity_id', 'commodity_code'], 'integer'],
            [['commodity_name', 'commodity_created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Commodities::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'commodity_id' => $this->commodity_id,
            'commodity_code' => $this->commodity_code,
            'commodity_created_date' => $this->commodity_created_date,
        ]);

        $query->andFilterWhere(['like', 'commodity_name', $this->commodity_name]);

        return $dataProvider;
    }
}
