<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Markets;

/**
 * MarketsSearch represents the model behind the search form about `app\models\Markets`.
 */
class MarketsSearch extends Markets
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['market_id', 'market_code'], 'integer'],
            [['market_name', 'market_location', 'market_created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Markets::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'market_id' => $this->market_id,
            'market_code' => $this->market_code,
            'market_created_date' => $this->market_created_date,
        ]);

        $query->andFilterWhere(['like', 'market_name', $this->market_name])
            ->andFilterWhere(['like', 'market_location', $this->market_location]);

        return $dataProvider;
    }
}
