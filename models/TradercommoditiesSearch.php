<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tradercommodities;


/**
 * TradercommoditiesSearch represents the model behind the search form about `app\models\Tradercommodities`.
 */
class TradercommoditiesSearch extends Tradercommodities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trader_id_no'], 'integer'],
            [['tradercommodities_id', 'commodity_id', 'trader_id', 'traderId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tradercommodities::find();
        $query_2 = Tradercommodities::find();
      

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('commodity');
        $query_2->joinWith('trader');
//         ->joinWith('trader')
//          ->joinWith('trader0')
//           ->joinWith('traderIdNo');
        
        
       

        // grid filtering conditions
//        $query->andFilterWhere([
//          
//            'trader_id_no' => $this->trader_id_no,
//        ]);
        
        $query->andFilterWhere(['like', 'commodities.commodity_name',$this->commodity_id]);
        $query_2->andFilterWhere(['like', 'trader.fName',$this->trader_id]);
//          ->andFilterWhere(['like', 'trader.fName', $this->trader_id])
//          ->andFilterWhere(['like', 'trader.sName', $this->traderId])
//          ->andFilterWhere(['like', 'trader.sName', $this->traderId]);
        

        return $dataProvider;
    }
}
