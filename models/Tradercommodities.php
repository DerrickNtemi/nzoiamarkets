<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tradercommodities".
 *
 * @property integer $tradercommodities_id
 * @property integer $commodity_id
 * @property integer $trader_id
 * @property integer $traderId
 * @property integer $trader_id_no
 *
 * @property Trader $trader
 * @property Trader $traderIdNo
 * @property Commodities $commodity
 * @property Trader $trader0
 */
class Tradercommodities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tradercommodities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commodity_id', 'trader_id', 'traderId', 'trader_id_no'], 'required'],
            [['commodity_id', 'trader_id', 'traderId', 'trader_id_no'], 'integer'],
            [['trader_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trader::className(), 'targetAttribute' => ['trader_id' => 'trader_id']],
            [['trader_id_no'], 'exist', 'skipOnError' => true, 'targetClass' => Trader::className(), 'targetAttribute' => ['trader_id_no' => 'id_No']],
            [['commodity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Commodities::className(), 'targetAttribute' => ['commodity_id' => 'commodity_id']],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tradercommodities_id' => 'Tradercommodities ID',
            'commodity_id' => 'Commodity name',
            'trader_id' => 'Trader fname',
            'traderId' => 'Trader sname',
            'trader_id_no' => 'Trader Id No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrader()
    {
        return $this->hasOne(Trader::className(), ['trader_id' => 'trader_id']);
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraderIdNo()
    {
        return $this->hasOne(Trader::className(), ['id_No' => 'trader_id_no']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommodity()
    {
        return $this->hasOne(Commodities::className(), ['commodity_id' => 'commodity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
 
}
