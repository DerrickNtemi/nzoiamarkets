<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trader".
 *
 * @property integer $trader_id
 * @property string $fName
 * @property string $sName
 * @property integer $id_No
 * @property string $date
 * @property string $phone
 * @property string $stall_No
 * @property string $passport_photo_no
 * @property integer $market_id
 *
 * @property Markets $market
 * @property Tradercommodities[] $tradercommodities
 * @property Tradercommodities[] $tradercommodities0
 * @property Tradercommodities[] $tradercommodities1
 */
class Trader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trader';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fName', 'sName','email', 'id_No', 'date', 'phone', 'stall_No', 'passport_photo_no','trader_commodity_id', 'market_id'], 'required'],
            [['id_No', 'market_id'], 'integer'],
            [['date'], 'safe'],
            [['fName', 'sName','email', 'stall_No', 'passport_photo_no'], 'string', 'max' =>30],
            [['phone'], 'string', 'min'=>12,'max' => 12],
            [['id_No'], 'unique'],
            [['id_No'], 'string','max'=>8,'min'=>6],
            [['stall_No'], 'unique'],
           
            [['passport_photo_no'], 'unique'],
            [['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markets::className(), 'targetAttribute' => ['market_id' => 'market_id']],
//            [['trader_commodity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Commodities::className(), 'targetAttribute' => ['trader_commodity_id' => 'commodity_name']],
            ['email','email']
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trader_id' => 'Trader ID',
            'fName' => 'First Name',
            'sName' => 'Second Name',
            'email' => 'E-mail',
            'id_No' => 'Id  No',
            'date' => 'Date',
            'phone' => 'Phone',
            'stall_No' => 'Stall  No',
            'passport_photo_no' => 'Passport Photo No',
            'market_id' => 'Market Name',
            'trader_commodity_id'=>'Trader commodities',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Markets::className(), ['market_id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getCommodities()
//    {
//        return $this->hasMany(Commodities::className(), [ 'commodity_name' => 'trader_commodity_id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradercommodities0()
    {
        return $this->hasMany(Tradercommodities::className(), ['trader_id_no' => 'id_No']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradercommodities1()
    {
        return $this->hasMany(Tradercommodities::className(), ['traderId' => 'trader_id']);
    }
}
