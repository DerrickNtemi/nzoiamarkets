<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;
use kartik\sidenav\SideNav;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use kartik\widgets\Spinner;
use richardfan\widget\JSRegister;

$url = Yii::$app->getUrlManager();
$item = Yii::$app->controller->id . "/" . Yii::$app->controller->action->id;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>

   
<?php $this->beginBody() ?>
<div class="menubar">
<?php
    NavBar::begin([
        'brandLabel' => 'E-NZOIA',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
  
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    </div>
 <div id="breadcrumbs">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>

<div class=" col-xs-5 col-sm-4 col-lg-3 menu">
<?=

    SideNav::widget([
        'type' => SideNav::TYPE_DEFAULT,
        'heading' => '',
        'items' => [
            ['url' => $url->createUrl(['/site/index']), 'label' => 'Home', 'icon' => 'th', 'active' => ($item == 'site/index')],
            
           
            [
                'label' => 'Traders',
                'icon' => 'th',
                'items' => [
                    ['label' => 'View Traders', 'icon' => 'list', 'url' => $url->createUrl(['/trader/index']), 'active' => ($item == 'trader/index')],
                    ['label' => 'Create Traders', 'icon' => 'list', 'url' => $url->createUrl(['/trader/create']), 'active' => ($item == '/trader/create')],
                ],
            ],
            [
                'label' => 'Markets',
                'icon' => 'th',
                'items' => [
                    ['label' => 'View Markets', 'icon' => 'list', 'url' => $url->createUrl(['/markets/index']), 'active' => ($item == 'markets/index')],
                    ['label' => 'Create Markets', 'icon' => 'list', 'url' => $url->createUrl(['/markets/create']), 'active' => ($item == 'markets/create')],
                ],
            ],
            
            [
                'label' => 'Commodities',
                'icon' => 'th',
                'items' => [
                    ['label' => 'View Commodities', 'icon' => 'list', 'url' => $url->createUrl(['/commodities/index']), 'active' => ($item == 'commodities/index')],
                    ['label' => 'Create Commodities', 'icon' => 'file', 'url' => $url->createUrl(['/commodities/create']), 'active' => ($item == 'commodities/create')],
                    //['label' => 'E-Learning', 'icon' => 'file', 'url' => 'http://library.kasneb.or.ke/', 'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'],
               ],
            ],
            [
                'label' => 'Trader Commodities',
                'icon' => 'th',
                'items' => [
                    ['label' => 'View Trader Commodities', 'icon' => 'list', 'url' => $url->createUrl(['/tradercommodities/index']), 'active' => ($item == '/tradercommodities/index')],
                  
                    ['label' => 'Add Trader Commodities', 'icon' => 'list', 'url' => $url->createUrl(['/tradercommodities/create']), 'active' => ($item == '/tradercommodities/create')],
                    
                ],
            ], 
            [
                'label' => 'User manager',
                'icon' => 'user',
                'items' => [
                    ['label' => 'View users', 'icon' => 'list', 'url' => $url->createUrl(['user/admin/index']), 'active' => ($item == 'user/admin/index')],
                  
                    ['label' => 'Create users', 'icon' => 'list', 'url' => $url->createUrl(['user/admin/create']), 'active' => ($item == 'user/admin/create')],
                    
                ],
            ], 
            
        ],
    ]);
    ?>
</div>
    
    <div class="col-xs-7 col-sm-8 col-lg-9">
      
        <?= $content ?>
    </div>




<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
