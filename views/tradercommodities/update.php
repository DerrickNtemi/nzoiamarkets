<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tradercommodities */

$this->title = 'Update Tradercommodities: ' . $model->tradercommodities_id;
$this->params['breadcrumbs'][] = ['label' => 'Tradercommodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tradercommodities_id, 'url' => ['view', 'id' => $model->tradercommodities_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tradercommodities-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
