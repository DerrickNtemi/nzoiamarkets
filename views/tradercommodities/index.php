<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TradercommoditiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tradercommodities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tradercommodities-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tradercommodities', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
               'attribute'=> 'commodity_id',
                'value'=>'commodity.commodity_name',
                
            ],
            [
                'attribute'=> 'trader_id',
                'value'=>'trader.fName',
            ],
            [
                'attribute'=>'traderId',
                 'value'=>'trader.sName',
                
            ],
            [
                'attribute'=>'trader_id_no',
                 'value'=>  'traderIdNo.id_No',
                
            ],

           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
