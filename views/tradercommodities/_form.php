<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Commodities;
use app\models\Trader;
/* @var $this yii\web\View */
/* @var $model app\models\Tradercommodities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tradercommodities-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <?= $form->field($model, 'commodity_id')->dropDownList(ArrayHelper::map(Commodities::find()->all(),'commodity_id','commodity_name'), ['prompt'=>'Select commodity'])?>
    </div>
   <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <?= $form->field($model, 'trader_id')->dropDownList(ArrayHelper::map(Trader::find()->all(),'trader_id','fName'), ['prompt'=>'Select trader first name'])?>
    </div> 
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
   <?= $form->field($model, 'traderId')->dropDownList(ArrayHelper::map(Trader::find()->all(),'trader_id','sName'), ['prompt'=>'Select trader second name'])?>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <?= $form->field($model, 'trader_id_no')->dropDownList(ArrayHelper::map(Trader::find()->all(),'id_No','id_No'), ['prompt'=>'Select trader id no'])?>
    </div>
   </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
