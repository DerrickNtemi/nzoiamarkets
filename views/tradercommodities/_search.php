<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TradercommoditiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tradercommodities-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tradercommodities_id') ?>

    <?= $form->field($model, 'commodity_id') ?>

    <?= $form->field($model, 'trader_id') ?>

    <?= $form->field($model, 'traderId') ?>

    <?= $form->field($model, 'trader_id_no') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
