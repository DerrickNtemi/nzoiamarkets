<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tradercommodities */

$this->title = $model->tradercommodities_id;
$this->params['breadcrumbs'][] = ['label' => 'Tradercommodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tradercommodities-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tradercommodities_id], ['class' => 'btn btn-primary']) ?>
        <!--?= Html::a('Delete', ['delete', 'id' => $model->tradercommodities_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tradercommodities_id',
            'commodity_id',
            'trader_id',
            'traderId',
            'trader_id_no',
        ],
    ]) ?>

</div>
