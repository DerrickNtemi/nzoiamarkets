<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tradercommodities */

$this->title = 'Create Tradercommodities';
$this->params['breadcrumbs'][] = ['label' => 'Tradercommodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tradercommodities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
