<?php

use yii\helpers\Html;
use yiii\web\session;


/* @var $this yii\web\View */
/* @var $model app\models\Trader */

$this->title = 'Create Trader';
$this->params['breadcrumbs'][] = ['label' => 'Traders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//var_dump($session['error_object']); die();
?>
<div class="trader-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('error-message')): ?>
    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error-message') ?></div>
<?php endif; ?>
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
   

</div>
