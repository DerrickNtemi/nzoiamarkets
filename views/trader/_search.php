<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TraderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trader-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'trader_id') ?>

    <?= $form->field($model, 'fName') ?>

    <?= $form->field($model, 'sName') ?>

    <?= $form->field($model, 'id_No') ?>

    <?= $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'stall_No') ?>

    <?php // echo $form->field($model, 'passport_photo_no') ?>

    <?php // echo $form->field($model, 'market_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
