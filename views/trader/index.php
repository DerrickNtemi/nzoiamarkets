<?php

use yii\helpers\Html;
use kartik\grid\GridView;





/* @var $this yii\web\View */
/* @var $searchModel app\models\TraderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Traders';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trader-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trader', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
   <p>
        <?= Html::a('Download', ['export'], ['class' => 'btn btn-primary btn-lg']) ?>
    </p>
           




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [ 'attribute'=>'market_id',
             'value'=>'market.market_name',
                
            ],
            
            //'trader_id',
            'fName',
            'sName',
            'email',
            'id_No',
            'date',
            'phone',
            'stall_No',
            'passport_photo_no',
            'trader_commodity_id',
         

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
