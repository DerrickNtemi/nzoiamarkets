<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Markets;
use app\models\Commodities;
use yii\web\Session;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Trader */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trader-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'fName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'sName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>


        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'id_No')->textInput() ?>
        </div>
    </div>

    <?php
    $commodities = Commodities::find()->all();
    $commoditiesList = ArrayHelper::map($commodities, 'commodity_name', 'commodity_name');
    ?>
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'stall_No')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'passport_photo_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'market_id')->dropDownList(ArrayHelper::map(Markets::find()->all(), 'market_id', 'market_name'), ['prompt' => 'Select the market']) ?>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model, 'trader_commodity_id[]')->dropDownList($commoditiesList,['multiple' => 'multiple'], ['prompt' => 'Select the commodity']) ?>
         
               
            </div>
        </div>
    </div>
</div>
<div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>



