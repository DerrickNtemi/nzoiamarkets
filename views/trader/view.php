<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trader */

$this->title = $model->trader_id;
$this->params['breadcrumbs'][] = ['label' => 'Traders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trader-view">

    <h1><?= Html::encode($this->title) ?></h1>  <?php if (Yii::$app->session->hasFlash('success-message')): ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
<?php endif; ?>



    

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->trader_id], ['class' => 'btn btn-primary']) ?>
        <!--?= Html::a('Delete', ['delete', 'id' => $model->trader_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'trader_id',
            'fName',
            'sName',
            'id_No',
            'date',
            'phone',
            'stall_No',
            'passport_photo_no',
            'market.market_name',
            'trader_commodity_id'
        ],
    ]) ?>

</div>
