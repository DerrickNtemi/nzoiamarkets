<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommoditiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commodities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodities-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Commodities', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Download', ['export'], ['class' => 'btn btn-primary btn-lg']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'commodity_id',
            'commodity_code',
            'commodity_name',
            'commodity_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
