<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Commodities */

$this->title = 'Create Commodities';
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
