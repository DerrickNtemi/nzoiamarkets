<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CommoditiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commodities-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'commodity_id') ?>

    <?= $form->field($model, 'commodity_code') ?>

    <?= $form->field($model, 'commodity_name') ?>

    <?= $form->field($model, 'commodity_created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
