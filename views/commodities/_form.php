<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Commodities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commodities-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <?= $form->field($model, 'commodity_code')->textInput() ?>
    </div>
   
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <?= $form->field($model, 'commodity_name')->textInput(['maxlength' => true]) ?>
    </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
