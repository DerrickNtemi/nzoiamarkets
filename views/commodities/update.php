<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Commodities */

$this->title = 'Update Commodities: ' . $model->commodity_id;
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->commodity_id, 'url' => ['view', 'id' => $model->commodity_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commodities-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
