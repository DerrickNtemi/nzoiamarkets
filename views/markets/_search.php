<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MarketsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="markets-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'market_id') ?>

    <?= $form->field($model, 'market_code') ?>

    <?= $form->field($model, 'market_name') ?>

    <?= $form->field($model, 'market_location') ?>

    <?= $form->field($model, 'market_created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
